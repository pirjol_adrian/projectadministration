package com.example.projectAdministration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectAdministrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectAdministrationApplication.class, args);
	}

}
