package com.example.projectAdministration.model;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@ToString
@Entity
public class User {
    @Id
    private Long id;
    private String username;
    private String password;
    private String role;

}
